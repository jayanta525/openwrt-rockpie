
  

![enter image description here](https://forum.radxa.com/uploads/default/original/1X/90f322e1c2b68fa612e2be9b92f0587b8f21726a.png)

# Radxa OpenWrt downloads

-  [Radxa Rock Pi E](https://jayanta525.gitlab.io/openwrt-rockpie/armv8)

    ![enter image description here](https://www.armbian.com/wp-content/uploads/2020/06/rockpie-300x169.png)

	- [ext4-sysupgrade](https://jayanta525.gitlab.io/openwrt-rockpie/armv8/openwrt-rockchip-armv8-radxa_rock-pi-e-ext4-sysupgrade.img.gz)
  
	- [squashfs-sysupgrade](https://jayanta525.gitlab.io/openwrt-rockpie/armv8/openwrt-rockchip-armv8-radxa_rock-pi-e-squashfs-sysupgrade.img.gz) (preferred)
  
	- [download directory](https://jayanta525.gitlab.io/openwrt-rockpie/armv8/)
	
- Radxa Rock Pi S

    ![enter image description here](https://www.armbian.com/wp-content/uploads/2019/11/rockpi-s-300x169.png)

	- coming soon

  

## Instructions

  

[https://wiki.radxa.com/RockpiE/OpenWRT](https://wiki.radxa.com/RockpiE/OpenWRT)

  

## Changelog

#### Date# 28-07-2020

  

- Updated to latest master
- Boost upto 1.5GHz
- Seperate feed repository
-  `opkg install` support